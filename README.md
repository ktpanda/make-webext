make_webext
===========

This is a utility to assist GreaseMonkey user script authors in converting scripts to a
full Firefox WebExtension. It is *not* a generic tool for converting arbitrary
userscripts, though many simple ones might work as-is.

It also contains functionality beyond what normal userscripts are capable of.

Requirements
------------

[Python 3.4+](https://www.python.org/downloads/)

Basic Usage
-----------

```
usage: make_webext.py [-h] [-b] [-o FILE|DIR] [-s FILE|DIR] [-u DIR] [-c DIR]
                      [-r]
                      files [files ...]

positional arguments:
  files                 Source files to process

optional arguments:
  -h, --help            show this help message and exit
  -b, --batch           Process multiple scripts. Output filenames are derived
                        from input filenames.
  -o FILE|DIR, --output FILE|DIR
                        Name of XPI file to create. In batch mode, XPI files
                        will be created in this directory
  -s FILE|DIR, --source FILE|DIR
                        Name of source zip file to create. In batch mode, zip
                        files will be created in this directory
  -u DIR, --unpack DIR  Create an unpacked version of the extension in DIR
                        that can be loaded via about:debugging
  -c DIR, --cache DIR   Directory to cache downloaded resources in
  -r, --refresh         Refresh all remote resources
```

Loading your extension
----------------------

# Temporarily

You can load an unpacked extension temporarily in any version of Firefox.

1. Build the extension with the `--unpack` argument and specify an output directory. You can also extract the files from the generated .xpi file.
2. Open `about:debugging` in Firefox.
3. Click "Load Temporary Add-on..."
4. Navigate to the directory your extension is unpacked in and open `manifest.json`.

Your extension is now installed for the current browser session. It will be gone if you restart Firefox.

# Permanently without signing

This method does not work in the official Firefox and Firefox Beta builds. You need to be
running Firefox Developer Edition, Firefox Nightly, or Firefox ESR.

1. Open `about:config`.
2. In the search box, enter `xpinstall.signatures.required`.
3. If the value is `true`, double-click it to set it to `false`.
4. Press Ctrl-O or select Open File from the menu.
5. Open the generated .xpi file.

# Signing your extension

Once your extension is complete, you can get it signed by Mozilla. You can choose to
publish it on addons.mozilla.org or keep it private. Follow the instructions on the [MDN
documentation](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/Distribution) to
sign your extension.

When uploading your extension, you will be asked to submit source code if you use "[a]ny
other tool that takes code or files, applies processing, and generates code or file(s) to
include in the add-on". Since make_webext qualifies as such a tool, you should select
"Yes" and upload a ZIP file created with the `--source` option. This source file contains
your original source, a copy of make_webext.py, any external resources, and auto-generated
build instructions.

Supported Standard Metadata
---------------------------

See [GreaseMonkey Documentation](https://wiki.greasespot.net/Metadata_Block).

| Key          | Notes       |
| ------------ | ----------- |
| `@name`, `@description`, `@version` | Copied straight to `manifest.json`. |
| `@icon`, `@require`, `@resource` | If a URL is provided, it will be downloaded and cached. For `@resource`, if the URL is preceded by `!`, it is added to `web_accessible_resources`. |
| `@include` | Maps to `content_scripts[#].include_globs`. |
| `@exclude` | Maps to `content_scripts[#].exclude_globs`. |
| `@match` | Maps to `content_scripts[#].matches`. Since this key is required by WebExtensions, but not by user scripts, it defaults to "<all_urls>" if not specified.  Any `@match` directive will automatically be added to the "permissions" key in the manifest. |
| `@namespace` | Only the domain of the url is considered. This sets the extension ID in `applications.gecko.id` to `<userscript filename>@<namespace domain>`. |
| `@noframes` | Sets `content_scripts[#].all_frames` to `false`. |
| `@run-at` | Valid values are `document-start`, `document-end`, `document-idle`, `background`, `common`, `resource`. See [Background Scripts](#background-scripts), [Common Scripts](#common-scripts), and [Inline Resources](#inline-resources). |

New Metadata
------------

| Key               | Notes       |
| ----------------- | ----------- |
| `@permission`     | Adds a [permission](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/permissions) to the manifest, either a match pattern or a special permission. |
| `@noblank`        | Sets `content_scripts[#].match_about_blank` to `false`. |
| `@filename`       | Defines the filename to be used for the script inside the extension. See [Multi-script Files](#multi-script-files).|
| `@manifest`       | See [Custom Manifest Data](#custom-manifest-data) |
| `@web-accessible` | Adds the current script or resource to `web_accessible_resources`. |
| `@keep-empty`     | Do not discard the current file even if it is empty. |


Unsupported Features
--------------------

`@grant none` is not supported. Content scripts never run in the same javascript context as page scripts.

Only a few of the `GM_` functions are supported. See the table below for equivalents.

| Function              | Equivalent          |
| --------------------- | ------------------- |
| `GM_getResourceURL`, `GM_getResourceText` | Implemented if any `@resource` directives with names are present. |
| `GM_xmlHttpRequest`   | [fetch()](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch) |
| `GM_getValue`, `GM_setValue` | [browser.storage](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage) |
| `GM_info`             | [browser.extension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/extension) |
| `GM_notification`     | [browser.notifications](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/notifications) |
| `GM_openInTab`        | [browser.tabs.create()](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/tabs/create) |
| `GM_setClipboard`     | [window.clipboard](https://w3c.github.io/clipboard-apis/) |
| `unsafeWindow`        | [window.wrappedJSObject](https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/wrappedJSObject) |

Multi-script Files
------------------

You can define multiple user scripts in one file by including multiple metadata headers. For example:

```
// ==UserScript==
// @name        Demo Userscript
// @filename    content_example.js
// @namespace   https://ktpanda.org/
// @match       *://example.com/*
// @run-at      document-end
// ==/UserScript==

console.log('running on example.com')

// ==UserScript==
// @filename    content_google.js
// @match       *://google.com/*
// @run-at      document-end
// ==/UserScript==

console.log('running on google.com')

```

The following keys apply to individual scripts: `@filename`, `@match`, `@include`,
`@exclude`, `@require`, `@run-at`, `@noframes`, `@noblank`, `@web-accessible`,
`@keep-emtpy`. All others apply globally to the extension.

Separate content scripts do not have access to anything defined in other scripts (except
common scripts - see below)

Background Scripts
------------------

All GreaseMonkey scripts are what are considered "content scripts" - they always run in
the context of a particular page. WebExtensions. however, can run scripts in the
"background" as well. In fact, many of the APIs in the `browser.` namespace only work from
a background script. For more information, see [the MDN documenation on background
scripts](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Anatomy_of_a_WebExtension#Background_scripts).

To run a script in the background, specify `@run-at background`. You _can_ define multiple
background scripts, but they are all run in the same context, so there's no real point to
it.

Background scripts cannot access variables in content scripts, and vice-versa. To
communicate between background and content scripts, use the message sending APIs in
[browser.runtime](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime)

Common Scripts
--------------

Common scripts are defined with `@run-at common`. Definitions in these scripts are made
available in all background and content scripts, so they are ideal for defining constants.

Inline Resources
----------------

You can include any text-based resources (e.g. HTML, CSS, JS) directly in your source
script by defining a header with `// ==Resource==`. These resources are not added to the
manifest at all (unless you specify `@web-accessible`).

```
// ==Resource==
// @filename options.html
// ==/Resource==
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="options.js"></script>
    <link rel="stylesheet" type="text/css" href="options.css">
  </head>
  <body>
    <div id="main"></div>
  </body>
</html>

// ==Resource==
// @filename options.css
// ==/Resource==

body {
    text-align: center
}
```

Custom Manifest Data
--------------------

The `@manifest` key can be used to add custom data to `manifest.json`.

Examples:

```
// @manifest blah = [1,2,3]
// Defines a top-level key "blah" and assigns it the value [1, 2, 3]
// -> {"blah": [1, 2, 3]}

// @manifest blah += [4,5,6]
// Extends 'blah' and adds 7, 8, 9 to the list
// -> {"blah": [1, 2, 3, 7, 8, 9]}

// @manifest blah -= [8]
// Removes 8 from the list
// -> {"blah": [1, 2, 3, 7, 8, 9]}

// @manifest        obj = {"a": {"b": {}, "c": [], "d": 2}}
// -> {"blah": [1, 2, 3, 7, 8, 9], "obj":{"a": {"b": {}, "c": [], "d": 2}}}

// @manifest        obj += {"y": [1, 2, 3]}
// -> {"blah": [1, 2, 3, 7, 8, 9], "obj":{"a": {"b": {}, "c": [], "d": 2}, "y": [1, 2, 3]}}

// @manifest        obj.q = "123"
// -> {"blah": [1, 2, 3, 7, 8, 9], "obj":{"a": {"b": {}, "c": [], "d": 2}, "y": [1, 2, 3], "q": "123"}}

// @manifest        obj.a.b += {"abc": [3,2,1], "def":1}
// -> {"blah": [1, 2, 3, 7, 8, 9], "obj":{"a": {"b": {"abc": [3, 2, 1], "def":1}, "c": [], "d": 2}, "y": [1, 2, 3], "q": "123"}}

// @manifest        obj.a.b -= "def"
// -> {"blah": [1, 2, 3, 7, 8, 9], "obj":{"a": {"b": {"abc": [3, 2, 1]}, "c": [], "d": 2}, "y": [1, 2, 3], "q": "123"}}
```
