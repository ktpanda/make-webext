#!/usr/bin/env python3

# make_webext.py - Converts a GreaseMonkey / TamperMonkey script into a Firefox WebExtension
#
# Copyright 2018 Katie Stafford (https://ktpanda.org)
#
# Redistribution and use in source and binary forms, with or without modification, are
# permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of
# conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
# of conditions and the following disclaimer in the documentation and/or other materials
# provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
# THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import copy
import json
import shutil
import zipfile
import hashlib
import urllib.request

try:
    # to get size of icon - will just assume "32" if not present
    from PIL import Image
except ImportError:
    Image = None

from urllib.parse import urlparse
from os.path import dirname, basename, join, exists, splitext
from collections import defaultdict, OrderedDict

from io import BytesIO

class AnnotatedString(str):
    @classmethod
    def get(cls, text, lineno):
        self = cls(text)
        self.lineno = lineno
        return self

class NoMetablockError(Exception):
    pass

class MetablockParseError(Exception):
    pass

class ResourceNotFoundError(Exception):
    pass

def get_extension_base_filename(filename):
    base = basename(filename)

    if base.endswith('.user.js'):
        return base[:-8]

    return splitext(base)[0]

def get_opt_meta(meta, key, default=None):
    lst = meta.get(key)
    if lst:
        return lst[0]
    return default

class Resource:
    def __init__(self, url=None, path=None, filename=None, data=None, is_remote=False):
        self.url = url
        self.path = path
        self.filename = filename
        self.data = data
        self.is_remote = is_remote

    @classmethod
    def get(cls, base_dir, cache_dir, url, force_losd=False):
        scheme, netloc, path = urlparse(url)[:3]
        if scheme == '':
            return cls(url, join(base_dir, url), basename(url))

        filename = basename(path)

        try:
            os.makedirs(cache_dir)
        except FileExistsError:
            pass

        cache_filename = re.sub(r'[^\w_\-\.]', lambda m: '-%x-' % ord(m.group(0)), url.replace('-', '--').replace('/', '-_'))
        path = join(cache_dir, cache_filename)
        if not force_losd and exists(path):
            return cls(url, path, filename, is_remote=True)

        print('fetching %s' % url)
        try:
            with urllib.request.urlopen(url) as urlf:
                data = urlf.read()
            with open(path, 'wb') as fp:
                fp.write(data)
        except urllib.error.URLError as e:
            raise ResourceNotFoundError('Unable to fetch %r' % url)

        return cls(url, path, filename, is_remote=True)

class WebExtBuilder:
    def __init__(self, source_userscript, cache_dir=None, output_filename=None, source_zip_filename=None, output_dir=None, force_load=False):
        self.source_filename = source_userscript
        self.cache_dir = cache_dir
        self.output_filename = output_filename
        self.source_zip_filename = source_zip_filename
        self.output_dir = output_dir

        self.base_dir = dirname(self.source_filename)

        self.extbase = get_extension_base_filename(self.source_filename)

        if self.output_filename is None:
            self.output_filename = join(self.base_dir, self.extbase + '.xpi')

        if self.cache_dir is None:
            self.cache_dir = join(self.base_dir, 'resource-cache')

        self.force_losd = force_load

        self.contents_by_url = {}
        self.contents_by_name = {}

        self.common_scripts = []
        self.content_scripts = []
        self.background_scripts = []
        self.manifest_extra = []
        self.web_accessible_resources = []
        self.permissions = set()
        self.grants = set()

        self.extname = None
        self.icon = None
        self.namespace = None
        self.description = None
        self.version = None

        self.content_map = {}

        self.manifest = OrderedDict()
        self.manifest_res = self.add_resource(Resource(filename='manifest.json', data=''))

    def build(self):
        self.build_scripts()
        self.build_manifest()
        self.parse_extra_manifest()
        self.write_contents()

    def add_resource(self, res):
        if res.url:
            self.contents_by_url[res.url] = res

        base, ext = splitext(res.filename)
        i = 2
        while res.filename in self.contents_by_name:
            res.filename = '%s.%d%s' % (base, i, ext)
            i += 1
        self.contents_by_name[res.filename] = res

        return res

    def fetch_resource(self, url):
        res = self.contents_by_url.get(url)
        if res:
            return res
        return self.add_resource(Resource.get(self.base_dir, self.cache_dir, url, self.force_losd))

    def build_scripts(self):
        print('converting %s...' % self.source_filename)

        with open(self.source_filename, 'r') as fp:
            self.source = source = fp.read().lstrip('\ufeff')

        lines = source.splitlines()

        rx_meta_header = re.compile(r'^//\s*=+(/?)(userscript|resource)=+', re.I)
        rx_param = re.compile(r'^//\s*@([\w\-]+)(.*)$')

        curmeta = None
        curlines = None

        in_metablock = False
        metablock_type = None
        for lineno, line in enumerate(lines, start=1):
            m = rx_meta_header.match(line)
            if in_metablock:
                if m:
                    if m.group(1) == '/':
                        if m.group(2).lower() != metablock_type:
                            print('%s:%d: WARNING: metablock end does not match start' % (self.source_filename, lineno))

                        if metablock_type == 'resource':
                            curmeta['run-at'] = ['resource']

                        in_metablock = False
                        curlines = []
                        continue

                m = rx_param.match(line)
                if m:
                    key = m.group(1)
                    val = AnnotatedString.get(m.group(2).strip(), lineno)
                    curmeta[key].append(val)
                elif line.strip():
                    print('%s:%d: WARNING: junk in metadata block: %r' % (self.source_filename, lineno, line))
            else:
                if m and m.group(1) != '/':
                    metablock_type = m.group(2).lower()
                    in_metablock = True

                    if curmeta is not None:
                        self.add_script(curmeta, ''.join(curlines))

                    curmeta = defaultdict(list)

                if curlines is not None:
                    curlines.append(line + '\n')

        if curmeta is None:
            raise NoMetablockError('ERROR: no metadata block found in %r!' % self.source_filename)

        self.add_script(curmeta, ''.join(curlines))

    def add_script(self, metablock, scriptdata):

        if self.extname is None:
            self.extname = get_opt_meta(metablock, 'name')

        if self.namespace is None:
            self.namespace = get_opt_meta(metablock, 'namespace')

        if self.description is None:
            self.description = get_opt_meta(metablock, 'description')

        if self.version is None:
            self.version = get_opt_meta(metablock, 'version')

        if self.icon is None:
            iconurl = get_opt_meta(metablock, 'icon')
            if iconurl is not None:
                self.icon = self.fetch_resource(iconurl)


        for res in metablock['resource']:
            web_accessible = False
            if res.startswith('!'):
                res = res[1:].strip()
                web_accessible = True

            m = re.match(r'(.*?)\s+(.*)', res)
            resname = None
            if m:
                resname = m.group(1)
                res = m.group(2)

            res = self.fetch_resource(res)
            if resname:
                self.content_map[resname] = res.filename

            if web_accessible:
                self.web_accessible_resources.append(res.filename)

        for url in metablock['require']:
            self.fetch_resource(url)

        self.permissions.update(metablock['permission'])
        self.grants.update(metablock['grant'])
        self.manifest_extra.extend(metablock['manifest'])

        # Done processing global stuff, now add the file itself if it is not empty

        if not scriptdata.strip() and not metablock['keep-empty']:
            return

        filename = get_opt_meta(metablock, 'filename')
        if filename is None:
            filename = (self.extname or self.extbase).replace(' ', '_') + '.js'

        script_res = self.add_resource(Resource(filename=filename, data=scriptdata))

        print('processing %s' % script_res.filename)

        runat = get_opt_meta(metablock, 'run-at', 'document_end').replace('-', '_')
        if runat not in ('background', 'document_start', 'document_end', 'document_idle', 'common', 'resource'):
            print('WARNING: invalid run-at value: %r' % runat)
            runat = 'document_end'

        if metablock['web-accessible']:
            self.web_accessible_resources.append(script_res.filename)

        if runat == 'common' or runat == 'background':
            lst = self.common_scripts if runat == 'common' else self.background_scripts
            lst.extend(self.contents_by_url[url].filename for url in metablock['require'])
            lst.append(script_res.filename)

            for key in ('include', 'exclude', 'match'):
                if metablock[key]:
                    print('WARNING: %s has no effect on %s script' % (key, runat))

        elif runat != 'resource':
            script_block = OrderedDict()
            script_block["js"] = [self.contents_by_url[url].filename for url in metablock['require']]
            script_block["js"].append(script_res.filename)

            script_block["run_at"] = runat

            if not metablock['match']:
                print('WARNING: No matches specified, matching <all_urls>!')
                metablock['match'] = ['<all_urls>']

            script_block['matches'] = metablock['match']
            self.permissions.update(metablock['match'])

            if metablock['include']:
                script_block['include_globs'] = metablock['include']
            if metablock['exclude']:
                script_block['exclude_globs'] = metablock['exclude']
            self.content_scripts.append(script_block)

            script_block["all_frames"] = not metablock['noframes']
            script_block["match_about_blank"] = not metablock['noblank']

    def build_manifest(self):
        self.manifest["manifest_version"] = 2
        self.manifest["name"] = self.extname or self.extbase
        self.manifest["description"] = self.description or ''
        self.manifest["version"] = self.version or '1.0'
        self.manifest["permissions"] = sorted(self.permissions)

        if self.icon:
            size = 48
            if Image:
                try:
                    im = Image.open(self.icon.path)
                    size = im.size[0]
                except Exception:
                    pass

            self.manifest["icons"] = {str(size): self.icon.filename}

        if self.namespace:
            domain = urlparse(self.namespace)[1]
        else:
            print('WARNING: no namespace given - assuming "example.com"')
            domain = 'example.com'

        self.manifest["applications"] = { "gecko": { "id": self.extbase + '@' + domain } }

        compat_script = ''

        if 'unsafeWindow' in self.grants:
            compat_script += 'const unsafeWindow = window.wrappedJSObject;\n'

        if self.content_map:
            compat_script += 'const content_map = ' + json.dumps(self.content_map, indent=True)
            compat_script += ';\nconst mapURL = name => browser.runtime.getURL(content_map[name]);\n'
            compat_script += 'const GM_getResourceUrl = name => Promise.resolve(mapURL(name));\n'
            compat_script += 'const GM_getResourceText = name => fetch(mapURL(name)).then(resp => resp.text())\n'

        if compat_script:
            res = self.add_resource(Resource(filename='gm_compat.js', data=compat_script))
            self.common_scripts.insert(0, res.filename)

        if self.background_scripts:
            self.background_scripts = self.common_scripts + self.background_scripts

        for script_block in self.content_scripts:
            script_block['js'] = self.common_scripts + script_block['js']

        if self.background_scripts:
            self.manifest['background'] = {'scripts': self.background_scripts}

        if self.content_scripts:
            self.manifest['content_scripts'] = self.content_scripts

        if self.web_accessible_resources:
            self.manifest['web_accessible_resources'] = self.web_accessible_resources

    def parse_extra_manifest(self):
        for spec in self.manifest_extra:
            m = re.match(r'(.*?)(=|\+=|-=)(.*)', spec)
            if not m:
                raise MetablockParseError('%s:%d: invalid manifest specification' % (self.source_filename, spec.lineno))

            try:
                val = json.loads(m.group(3), object_pairs_hook=OrderedDict)
            except ValueError as e:
                raise MetablockParseError('%s:%d: cannot parse JSON value: %s' % (self.source_filename, spec.lineno, e))

            oper = m.group(2)
            keypart = m.group(1).strip()
            if not keypart:
                raise MetablockParseError('%s:%d: invalid manifest specification' % (self.source_filename, spec.lineno))

            keylst = keypart.split('.')
            not_present = object()

            parent = child = self.manifest
            for i, key in enumerate(keylst):
                islast = i == len(keylst) - 1
                parent = child
                if isinstance(parent, dict):
                    try:
                        child = parent[key]
                    except KeyError:
                        child = not_present
                        if not islast:
                            raise MetablockParseError('%s:%d: %s: key not present (%s)' % (self.source_filename, spec.lineno, '.'.join(keylst[:i+1]), ', '.join(parent.keys())))

                elif isinstance(parent, list):
                    try:
                        child = parent[int(key)]
                    except ValueError:
                        child = not_present
                        if not islast:
                            raise MetablockParseError('%s:%d: %s: expected integer key' % (self.source_filename, spec.lineno, '.'.join(keylst[:i+1])))
                    except IndexError:
                        child = not_present
                        if not islast:
                            raise MetablockParseError('%s:%d: %s: index out of range (length=%d)' % (self.source_filename, spec.lineno, '.'.join(keylst[:i+1]), len(parent)))
                else:
                    raise MetablockParseError('%s:%d: %s: expected list or dict, got %s' % (self.source_filename, spec.lineno, '.'.join(keylst[:i]), type(parent).__name__))

            try:
                if oper == '=':
                    newval = val
                elif oper == '+=':
                    if isinstance(child, dict) and isinstance(val, (dict, OrderedDict)):
                        newval = OrderedDict(child)
                        newval.update(val)
                    else:
                        newval = child + val
                elif oper == '-=':
                    if isinstance(child, dict) or isinstance(child, list):
                        if isinstance(val, list):
                            rmvals = val
                        else:
                            rmvals = [val]

                        if isinstance(child, dict):
                            newval = dict(child)
                            for val in rmvals:
                                newval.pop(val, None)
                        else:
                            newval = list(child)
                            for val in rmvals:
                                try:
                                    newval.remove(val)
                                except ValueError:
                                    pass
                    else:
                         newval = child - val

            except TypeError:
                if child is not_present:
                    raise MetablockParseError('%s:%d: %s: key not found' % (self.source_filename, spec.lineno, keypart))
                else:
                    raise MetablockParseError('%s:%d: incompatible types for %r: %s %s %s' % (self.source_filename, spec.lineno, oper, type(child).__name__, oper, type(val).__name__))

            if isinstance(parent, dict):
                parent[keylst[-1]] = newval

            elif isinstance(parent, list):
                parent[int(keylst[-1])] = newval

    def write_resources(self, zf, output_dir, extonly=False):
        for res in self.contents_by_name.values():
            if res.data is not None:
                if extonly:
                    continue
                zf.writestr(res.filename, res.data)
                if output_dir:
                    with open(join(output_dir, res.filename), 'wb' if isinstance(res.data, bytes) else 'w') as fp:
                        fp.write(res.data)

            else:
                zf.write(res.path, res.filename)
                if output_dir:
                    shutil.copy2(res.path, join(output_dir, res.filename))

    def write_contents(self):
        self.manifest_res.data = json.dumps(self.manifest, indent=True)

        if self.output_dir:
            try:
                os.makedirs(self.output_dir)
            except FileExistsError:
                pass

        zf = zipfile.ZipFile(self.output_filename, 'w', zipfile.ZIP_DEFLATED)
        self.write_resources(zf, self.output_dir)
        zf.close()

        print('created %s' % self.output_filename)

        me = basename(__file__)

        if self.source_zip_filename:
            zf = zipfile.ZipFile(self.source_zip_filename, 'w', zipfile.ZIP_DEFLATED)

            sourcefn = basename(self.source_filename)

            message = '%s\n' % self.manifest['name']
            message += '%s\n\n' % ('=' * len(self.manifest['name']))
            message += 'This extension is built using %s.\n' % (me)
            message += '\n'
            message += 'Build prerequisites\n'
            message += '-------------------\n'
            message += '\n'
            message += 'Python 3.4+\n'
            message += '\n'
            message += 'Build instructions\n'
            message += '------------------\n'
            message += '\n'
            message += 'To build this extension, run:\n\n'
            message += './%s %s\n\n' % (me, sourcefn)
            zf.writestr('README', message)

            zf.writestr(sourcefn, self.source)
            zf.write(__file__, me)

            self.write_resources(zf, self.output_dir, True)

            zf.close()

            print('created %s' % self.source_zip_filename)

        print()

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('files', nargs='+', help='Source files to process')
    p.add_argument('-b', '--batch', action='store_true', help='Process multiple scripts. Output filenames are derived from input filenames.')
    p.add_argument('-o', '--output', metavar='FILE|DIR', help='Name of XPI file to create. In batch mode, XPI files will be created in this directory')
    p.add_argument('-s', '--source', metavar='FILE|DIR', help='Name of source zip file to create. In batch mode, zip files will be created in this directory')
    p.add_argument('-u', '--unpack', metavar='DIR', help='Create an unpacked version of the extension in DIR that can be loaded via about:debugging')
    p.add_argument('-c', '--cache', metavar='DIR', help='Directory to cache downloaded resources in')
    p.add_argument('-r', '--refresh', action='store_true', help='Refresh all remote resources')
    args = p.parse_args()

    if args.batch:
        for fn in args.files:
            extbase = get_extension_base_filename(fn)

            output_dir = None
            if args.unpack:
                output_dir = join(args.unpack, extbase + '-unpack')

            xpifn = extbase + '.xpi'

            if args.output:
                output_fn = join(args.output, xpifn)
            else:
                output_fn = join(dirname(fn), xpifn)

            if args.source:
                source_fn = join(args.source, extbase + '-source.zip')
            else:
                source_fn = None

            try:
                wxb = WebExtBuilder(args.files[0], cache_dir=args.cache, output_filename=output_fn, output_dir=output_dir, force_load=args.refresh, source_zip_filename=source_fn)
                wxb.build()
            except Exception:
                traceback.print_exc()
                print()
    else:
        if len(args.files) > 1:
            print('ERROR: processing multiple files requires --batch')
            p.print_help()
            return
        wxb = WebExtBuilder(args.files[0], cache_dir=args.cache, output_filename=args.output, output_dir=args.unpack, force_load=args.refresh)
        wxb.build()

if __name__ == '__main__':
    main()
